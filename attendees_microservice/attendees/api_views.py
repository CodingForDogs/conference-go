from django.http import JsonResponse

# from events.models import Conference
from .models import Attendee, ConferenceVO
from common.json import ModelEncoder

# from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeesListEncoder(ModelEncoder):
    # """A class to encode a list of the attendees."""

    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    # """A class to encode the details of an Attendee."""

    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    # response = []
    # attendees = Attendee.objects.all()
    # for attendee in attendees:
    #     response.append(
    #         {
    #             "name": attendee.name,
    #             "href": attendee.get_api_url(),
    #         }
    #     )
    """
    attendees = Attendee.objects.filter(
        conference=Conference.objects.get(id=conference_id)
    )
    return JsonResponse(attendees, encoder=AttendeesListEncoder, safe=False)
    """
    if request.method == "GET":
        attendees = Attendee.objects.filter(
            attendees=Attendee.objects.filter(conference=conference_vo_id)
        )
        return JsonResponse(
            attendees, encoder=AttendeesListEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )

    # return JsonResponse({"attendees": response})


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # response = []
    # attendees = Attendee.objects.all()
    # for attendee in attendees:
    #     response.append(
    #         {
    #             "email": attendee.email,
    #             "name": attendee.name,
    #             "company_name": attendee.company_name,
    #             "created": attendee.created,
    #             "conference": {
    #                 "name": attendee.conference.name,
    #                 "href": attendee.get_api_url(),
    #             },
    #         }
    #     )
    # return JsonResponse({"attendees": response})
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if "conference" in content:
            try:
                content["conference"] = ConferenceVODetailEncoder.objects.get(
                    content["conference"]
                )
            except ConferenceVODetailEncoder.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid conference id"},
                    status=400,
                )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
