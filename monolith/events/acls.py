from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

# import requests
import json

from .keys import PEXELS_API_KEY


def get_picture_url(city, state):
    """gets a url from pexels for the city and state passed in
    and returns the url in a dict.
    """
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city} {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=params)
    pexels_url = json.loads(response.content)
    if pexels_url["photos"]:
        picture_url = {
            "picture_url": pexels_url["photos"][0]["url"],
        }
    else:
        picture_url = {
            "picture_url": None,
        }
    return picture_url
